# Introduction

This is a test automation framework built based on Gradle and Groovy.

## Dependency

* gradle - Build tool
* groovy - Programming language
* cucumber - BDD tool
* junit/hamcrest - Test framework
* groovy-wslite - Web service
* wiremock - Mock and stub
* jsch - SFTP
* wss4j - Web service security for java
* sl4j - Logging
* snakeYaml - Data structure tool

Please find details in build.gradle

## Cucumber Test

Unix
```
gradlew cucumberTest -Dcucumber.options="-tags @soap"
```

## Test Scope

Basically, there are five parts below I've been implemented in the tests:
1. SOAP
2. REST
3. Mock
4. SFTP
5. Wss4j (SOAP)
